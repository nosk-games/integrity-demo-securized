﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameStats
    {
        private static string FILENAME = "game.sav";
        private static readonly string PasswordHash = "P@@Sw0rd";
        private static readonly string SaltKey = "S@LT&KEY";
        private static readonly string VIKey = "@1B2c3D4e5F6g7H8";

        public static int coins = 0;
        public static int maxScore = 0;

        public static void save()
        {
            string path = Application.persistentDataPath + "/" + FILENAME;
            string data = coins.ToString() + Environment.NewLine + maxScore.ToString();

            string encryptedData = encrypt(data);

            using (StreamWriter writer = new StreamWriter(path, false))
            {
                writer.Write(encryptedData);
                writer.Flush();
            }

        }

        public static void load()
        {
            string path = Application.persistentDataPath + "/" + FILENAME;
            Debug.Log(path);
            string strCoins = "";
            string strScore = "";
            string rawData;
            try
            {
                using (StreamReader reader = new StreamReader(path, false))
                {
                    rawData = reader.ReadToEnd();
                }

                string decryptedData = decrypt(rawData);

                string[] data = decryptedData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                strCoins = data[0];
                strScore = data[1];

            }
            catch(Exception e)
            {
                Debug.Log("Save file not found");
            }

            if (!string.IsNullOrEmpty(strCoins))
            {
                coins = int.Parse(strCoins);
            }

            if (!string.IsNullOrEmpty(strScore))
            {
                maxScore = int.Parse(strScore);
            }

        }

        private static string encrypt(String data)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(data);

            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);

        }

        private static string decrypt(String data)
        {

            byte[] cipherTextBytes = Convert.FromBase64String(data);
            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }
    }

}
