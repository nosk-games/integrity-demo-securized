﻿using System;


namespace Assets.Scripts
{
    [Serializable]
    public  class PlayerScore
    {
        public string user;
        public int score;
    }
}
