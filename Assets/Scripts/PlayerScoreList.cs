﻿using System;

namespace Assets.Scripts
{
    [Serializable]
    class PlayerScoreList
    {
        public PlayerScore[] items;
    }
}
