﻿using Assets.Scripts;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class Scoreboard : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnEnable()
    {
        StartCoroutine(UpdateScore());
    }


    IEnumerator UpdateScore()
    {
        UnityWebRequest scoreboardRaw = UnityWebRequest.Get("https://securescoreboard.dirtydevelopers.org/score");
        yield return scoreboardRaw.Send();
        string text = System.Text.Encoding.ASCII.GetString(scoreboardRaw.downloadHandler.data);
        PlayerScoreList playerScores = JsonUtility.FromJson<PlayerScoreList>("{\"items\":"+text+"}");
    
        string boardText = "";
        foreach (PlayerScore ps in playerScores.items)
        {
            boardText += "[" + ps.score + "] " + ps.user + "\n";
        }

        transform.Find("ranking").GetComponent<UnityEngine.UI.Text>().text = boardText;
    }
}
